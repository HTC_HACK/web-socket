package uz.pdp.springwebsocket.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data

//Asadbek Xalimjonov 3/4/22 2:56 PM

public class ChatMessage {


    private String content;
    private String sender;

    private MessageType type;

    public enum MessageType {
        CHAT, LEAVE, JOIN
    }

}
